const express = require('express');
const cors = require('cors');
const { connectDB } = require('./src/database');


require('dotenv').config();
const server = express();

server.use(cors());
server.use(express.urlencoded({ extended: true }));
server.use(express.json());

server.use('/', require('./src/routes/routes'));

// Nos conectamos a la BD
connectDB();


// Corremos el servidor
server.listen(process.env.PORT, () => console.log('Servidor corriendo en el puerto '+process.env.PORT));
