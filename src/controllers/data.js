const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');

const Clientes = require('../models/clientes');
const Familias = require('../models/familias');
const Productos = require('../models/producto');
const Users = require('../models/user');
const Ventas = require('../models/ventas');
const Creditos = require('../models/creditos');
const clientes = require('../models/clientes');

require('dotenv').config();

const controller = {};

const crearToken = (usuario, salt, expire) => {
  const {
    id, name, lastName, email, role,
  } = usuario;
  return jwt.sign({
    id, name, lastName, email, role,
  }, salt, { expiresIn: expire });
};

controller.buscarUsuarioPorId = async (req, resp) => {

 try{

  const { id } = req.params;

  const user =  await Users.findById( id );
  
  if(user != null) { 

    user.password = ':)';

    return resp.json({
      ok: true,
      message: 'Se encontró el usuario',
      data: user
    });

  }

  return resp.json({
    ok: false,
    message: 'No se encontró el usuario',
  });

 } catch (error) {

  console.log( error );
  
  return resp.json({
    ok: false,
    message: 'No se encontró el usuario',
    error
  });
 
}

}
controller.nuevoUsuario = async (req, res) => {

  const { email, password } = req.body;
  console.log( req.body );

  try{

    const usuarioExiste = await Users.findOne({ email });

    if (!usuarioExiste) {
      const salt = await bcryptjs.genSalt(10);
      req.body.password = await bcryptjs.hash(password, salt);
      const usuario = new Users(req.body);
      usuario.save();

      delete usuario.password
      return res.json({
        ok: true,
        message: 'Se creo el usuario',
        data: usuario,
      })
  
        }
      return res.json({
        ok: false,
        message: 'El usario ya existe',
      });
    }catch(error){


    console.log(error);

    return res.json({
      ok: false,
      message: 'Error de servidor',
      error: error,

    })

  }

}

controller.autenticarUsuario = async (req, res) => {

  const { email, password } = req.body;

  try {
  
    const user = await Users.findOne({ email });
  
    if (user === null) {
      return res.json({
        ok: false,
        message: 'No se encontro el usario',
      });
    }
    const usuarioCorrecto = await bcryptjs.compare(password, user.password);
    if (!usuarioCorrecto) {
      return res.json({
        ok: false,
        message: 'No se encontró el usuario',
      });
    }
    return res.json({
      ok: true,
      message: 'Se encontro el usuario',
      token: crearToken(usuarioCorrecto, process.env.salt, '24h'),
      user,
    });
  } catch (e) {
    return res.json({
      ok: false,
      message: 'No se encontrò el usuario',
    });
  }
};

controller.eliminarUsuario = async(req, resp) => {
  const { id } = req.params;
  
  try{

    const usuario = await Users.findById(id);
    if(!usuario) {

      return resp.json({
        ok: false,
        message: 'No se encontro el usuario'
      });

    }else {

     const usersDelete = await Users.findByIdAndDelete( id );
     console.log(usersDelete);
     return resp.json({
        ok: true,
        message: 'Se elimino el usuario'
      });
    }
  }catch (e) {

    console.log(e);
    return resp.json({
      ok: false,
      message: error
    });

  }
}
controller.obtenerUsuarios = async (req, res) => {
  try {

    const usuarios = await Users.find().exec();
    res.send(usuarios);
  } catch (error) {
    
    console.log(error);
    
    return res.json({
      ok:fale,
      message: error
    });
  }
};

controller.actualizarUsuaurio = async (req, resp) => { 

  const { id } = req.params;
  const  body = req.body;

  try {

   const  userInfoDB = await Users.findById( id );
   
   if( userInfoDB != null){

      const userEdited = await Users.findByIdAndUpdate( id, body );

      return resp.json({
        ok: true,
        message: 'Se actualizo la información',
        data: userEdited
      });
    
   }

   return resp.json({
     ok: false,
     message: 'No se encontró el usuario'
   })

  }catch (error) {

    console.log(error);
    
    return resp.json({
      ok: false,
      message: 'No se pudo conectar',
      error
    });
  }

}

// terminan las acciones de los usuarios
controller.obtenerProductos = async (req, res) => {
  try {

    const productosDB = await Productos.find();
    
    res.json({
      ok: true,
      message: 'Productos encontrados',
      productos: productosDB,
    });

  } catch (error) {
    
    console.log(error);
  }
};

// eliminar producto por el ID



controller.eliminarProductoPorId = async (req, resp) => {

  const { id} = req.params;
  console.log(id)
  try { 

    const producto = await Productos.findById( id );

    if( !producto ) {

      return resp.json({
        ok:false,
        message:'el producto no se encuentra registrado',
        staus:301
      });

    }else {

      const productoDeleted = await Productos.findByIdAndDelete( id );

      return resp.json({
        ok: true,
        message: 'Se elimino el producto',
        data: productoDeleted
      });
    }

  }catch (error) {

    console.log(error)
    return resp.json({
      ok: false,
      message: 'Error de conexion',
      error
    });
  }

}

controller.obtenerProductoPorId = async (req, res) => {
  const { idProducto } = req.body;
  try {

    const producto = await Productos.findById(idProducto);

    res.json({
      ok: true,
      message: 'Se encontro el producto',
      producto,
    });
  } catch (error) {

    console.log(error);
  }
};

controller.obtenerProductoPorNombre = async (req, res) => {
  const { nombre } = req.body;
  try {

    const producto = await Productos.find({ nombre });
    res.send(producto);
  
  } catch (error) {

    console.log(error);
  }
};

controller.obtenerFamilias = async (req, res) => {
  try {

    const familiasResult = await Familias.find();
    res.send(familiasResult);
  } catch (error) {
    console.log(error);
  }
};

controller.nuevoProducto = async (req, res) => {
  try {
    
    const { body} = req;
    console.log( body, "Registro nuevo producto" );

    const nuevoProducto = new Productos(body);
    nuevoProducto.save();
 
    return res.json({
      ok: true,
      message: 'Se creo el producto',
      data: nuevoProducto,
    });
 
  } catch (error) {
    throw new Error('Error:', error);
  }
};


controller.actulizarProducto = async (req, resp) => {
  try{

    const {id} = req.params;
    const { body } = req;

    console.log( body, Date.now() );
    //log de la entrada del request

    const productFind = await Productos.findById({_id: id});    
    // console.log(productFind, "productFind")
    if(!productFind){

      return resp.json({
        ok: false,
        message: 'ID no valido',
      });
    }

    const respUpdateProduct = await Productos.findByIdAndUpdate({_id: id}, body, {new:true});
    // console.log(respUpdateProduct, "respUpdateProduct")
    
    if( respUpdateProduct == null ){
      
      return resp.json({
        ok: false,
        message: 'No se actualzó el producto',
      });
    }

    //respuesta exitosa   
    return resp.json({
      ok: true,
      message: 'Se actualzó el producto',
      data:respUpdateProduct
    });

    
  } catch (error) {
    console.log(error, Date.now());
   return  resp.json({
      ok: false,
      message: 'Hubo un error: ' + error.message,
      error
    });

  }
};
controller.nuevaFamilia = async (req, res) => {
  const { codigo } = req.body;
  try {
 
    const familia = await Familias.find({ codigo });
 
    if (familia.length === 0) {
 
      const createdFamily = new Familias();
      createdFamily.save();
      res.send(createdFamily);
 
    } else {
 
      res.send('La familia ya existe');
    }
  } catch (error) {
    console.log(error);
  }

};

controller.eliminarFamilia = async (req, res) => {
  const { id } = req.body;

  try {

    const producto = await Familias.findById(id);

    if (!producto) {

      res.send('La familia no existe');

    } else {

      await Familias.findByIdAndDelete(id);
      res.send('Se elimino la familia');
    }

  } catch (error) {

    console.log(error);
  }
};

controller.nuevaVenta = async (req, res) => {
  try {

    console.log( req.body, "Body req nueva venta" );

    if(req.body.productos[0] == 'abono' ){
      const ventaCreada = new Ventas(req.body);
      const ventaPost = await ventaCreada.save();
    }else {
      for await (const producto of req.body.productos) {
        const { _id } = producto;
        const articulo = await Productos.findById(_id);
        
        if (producto.cantidadProducto > articulo.cantidadActual) {
      
          res.json({
            ok: false,
            message: 'La cantidad es mayor',
          });
        } else {
          
          await Productos.findByIdAndUpdate(_id, {cantidadActual:  articulo.cantidadActual - producto.cantidadProducto });
          const productUo = await Productos.findById(_id);
  
          const ventaCreada = new Ventas(req.body);
          const ventaPost = await ventaCreada.save();
        
          res.json({
            ok: true,
            message: 'Se registro la venta',
            data: ventaPost,
          });
        }
      }
    }

  } catch (error) {
    console.log(error)
    res.json({
      ok: false,
      message: error,
    });
  }
};

controller.nuevoCliente = async (req, res) => {

  console.log(req.body, "req body add cleintes");
  const nuevoCliente = new Clientes(req.body);
 
  try {

   const clienteNuevo = await nuevoCliente.save();
    
    if (nuevoCliente != null) {
    
      res.json({
        ok: true, 
        message: 'Se registro el cliente',
        data: clienteNuevo,
      });
    
    } else {
    
      res.json({
        ok: false,
        message: 'No se registro el cliente',
      });
    }

  } catch (error) {
    
    console.log(error);
    res.json({
      ok: false,
      error,
    });
  }
};

controller.obtenerClientes = async (req, res) => {
  try {

    const clientesResp = await Clientes.find();
    
    if (clientesResp == null || Clientes.length === 0) {
    
      res.json({
        ok: false,
        message: 'No hay clientes registrados',
        status: 205,
      });
    } else {
    
      res.json({
        ok: true,
        message: 'Se encontraron los clientes',
        status: 200,
        clientes: clientesResp,
      });
    }

  } catch (error) {
    
    console.log(error);
    res.json({
      ok: true,
      message: 'No se pudo conectar',
      error,
      status: 500,
    });
  }
};

controller.actualizarCliente = async (req, res) => {
  const { _id } = req.body;
  
  try {
  
    await Clientes.findByIdAndUpdate(_id, req.body);
    res.json({
      message: 'Se actualizo el cliente',
    });

  } catch (error) {
    console.log(error);
  }
};

controller.cliente = async (req, res) => {
  const { id } = req.params;
  
  try {
  
    const response = await Clientes.findById(id);
    res.send(response);
  
  } catch (error) {
    console.log(error);
  }

};

// Este metodo crea un nuevo credito, con los parametros necesarios
// controller.nuevoCredito = async (req, resp) => {
//   try {
  
//     for await (const producto of req.body.productos) {
//       const { _id } = producto;
//       const articulo = await Productos.findById(_id);
  
//       if (producto.cantidadProducto > articulo.cantidad) {
  
//         resp.json({
//           ok: false,
//           message: 'La cantidad es mayor a la que hay',
//           status: 500,
//         });

//       } else {
//         // articulo.cantidad = articulo.cantidad - producto.cantidadProducto;
//         articulo.cantidad -= producto.cantidadProducto;
//         await articulo.save();
//       }
//     }
//     try {
        
//         const nuevoCliente = new clientes(req.body)
//         await nuevoCliente.save()

//         if( nuevoCliente != null ) {
//             return res.json({
//                 ok: true,
//                 message: "Se registro el cliente",
//                 data: nuevoCliente
//             });

//         }else {

//             return res.json({
//                 ok: false,
//                 message: "No se registro el cliente"
//             })
//         }

//     } catch (error) {
//         console.log(error);
//         return res.json({
//             ok: false,
//             error
//         })
//     }
// }catch (error) {
//         console.log(error);
//         return res.json({
//             ok: false,
//             error
//         })
//     }
// };

controller.obtenerClientes = async (req, resp) => {
    
    try{

        const clientesResp = await clientes.find();

        if( clientesResp == null || clientes.length == 0 ) {

            return resp.json({
                ok: false,
                message: "No hay clientes registrados",
                status: 205
            });

        }else {

            return resp.json({
                ok: true,
                message: "Se encontraron los clientes",
                status: 200,
                clientes: clientesResp
            });
        }

    }catch (error) {    


        console.log(error )

        return resp.json({
            ok: false,
            message: "No se pudo conectar",
            error,
            status: 500
        })
    }
};

controller.eliminarCliente = async (req, resp) => {

    const { id } = req.params;

    try{

      const client = await clientes.findById( id );
        
      if(client == null){
        
        return resp.json({
        
            ok:false,
            message: "No se encontro el cliente",
            status: 500    
         });

        }

        if( client != null  ){
        
          const clienteDeleted =  await clientes.findByIdAndDelete( id );

            return resp.json({
                ok:true,
                message: 'Se elimino el cliente',
                data: clienteDeleted,
                status: 200
            });
        }   

    }catch (error) {

        return resp.json({
            ok:false,
            message: 'Fallo de conexión',
            error,
            status: 500
        });


    }
};

// Este metodo crea un nuevo credito, con los parametros necesarios

controller.nuevoCredito = async (req, resp) => {

    try{

        for await ( const producto of req.body.productos ) {

            const { _id }  = producto;

            const articulo = await Productos.findById( _id ); 

            if( producto.cantidadProducto > articulo.cantidadActual  ){

                return resp.json({ 
                    ok: false,
                    message: "La cantidad es mayor a la que hay",
                    status: 500
                });

            }else {
                //descuenta los productos del stock 
                articulo.cantidadActual =   producto.cantidadActual -producto.cantidadProducto;
                await articulo.save()
            }

        }
    
        try {

            const newCredito = new Creditos( req.body);
            
            await newCredito.save();
        
                if(newCredito){
                  
                  return resp.json({
                      ok: true,
                      message: "Se regitro el credito",
                      credito: newCredito,
                      status: 200
                  });

                }

                }catch(error) {
                
                  console.log(error)
                    return resp.json({
                        ok: false,
                        message: "No se pudo registar el credito",
                        error,
                        status: 500
                    });
            
        }

    }catch (error) {

        console.log( error );

        return resp.json({
            ok: false,
            message: "No se pudo conectar",
            error, 
            status: 500
        });



  //      return resp.json({
  //         ok: true,
  //         message: 'Se regitro el credito',
  //         credito: newCredito,
  //         status: 200,
  //       });
  //     }
  //    catch (error) {
  //     resp.json({
  //       ok: false,
  //       message: 'No se pudo registar el credito',
  //       error,
  //       status: 500,
  //     });
  //   } catch (error) {
  //   console.log(error);
  //   resp.json({
  //     ok: false,
  //     message: 'No se pudo conectar',
  //     error,
  //     status: 500,
  //   });
  // }
  };

};
// este metodo nos retorna todos los creditos creados con el paso anterior
controller.verTodosLosCreditos = async (req, res) => {
  

  try {
    // crear el update de los abonos para llevar el restante
    const credits = await Creditos.find({status: 'Abierto'}).populate('cliente').exec();                         

    if (credits.length > 0  ) {
    
      res.json({
        ok: true,
        message: 'Se encontraron los creditos',
        creditos: credits,
        status: 200,
      });

    } else {
      
      res.json({
        ok: false,
        message: 'No se encontraron los creditos',
        status: 404,
      });
    }

  } catch (error) {
    console.log(error);
    res.json({
      ok: false,
      message: 'No se pudo conectar',
      error,
      status: 500,
    });
  }
};
controller.buscarAbonoPorId = async (req, resp) => {
  const {id} = req.params
  
  try {
    

   const creditoId = await Creditos.findById( id ).populate('cliente');
    
   if(creditoId != null ){

    return resp.json({
      ok:true,
      message:'Se encontrò el credito', 
      credito: creditoId,
      status: 200
    });

    
  }else {
   
    return resp.json({
      ok:false,
      message:'No se encontrò el credito con ese ID',
      status: 300
    });
    
  }
  
  }catch (error) {

    console.log(error);

    return resp.json({
      ok:false,
      message: 'Error de servidor',
      error,
      status:500
    });

  }

};
controller.agregarAbono = async (req, resp) => {
  try {

    const { id } = req.params;
    const  { body } = req;

    console.log(id, "id abonos"); 
    console.log(body, "Body abonos")
    
    const abonoSaved = await Creditos.findByIdAndUpdate(id, { $push: { pagos: { abono: body }} });
    const abonoSavedRestante  = await Creditos.findByIdAndUpdate(id, { restante: body.restante }, { new:true });
    // agregar conidcion de que si el restante es 0 se quite de la bitacora
    if(abonoSavedRestante != null) {
      resp.json({
        ok: true,
        message: 'Se agrego el abono',
        status: 201,
        data: abonoSavedRestante
      });
    }

  } catch (error) {
    console.log(error);
    resp.json({
      ok: false,
      message: error,
      status: 500,
    });
  }

};

module.exports = controller;
