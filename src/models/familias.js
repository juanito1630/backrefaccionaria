const mongoose = require('mongoose');

const schemaFamilias = new mongoose.Schema({
  codigo: { type: String, trim: true, required: true },
  descripcion: { type: String, trim: true, required: true },
  creador: { type: mongoose.SchemaTypes.ObjectId, ref: 'users' },
  fecha: { type: Date, dafault: Date.now() },
});

module.exports = mongoose.model('familias', schemaFamilias);
