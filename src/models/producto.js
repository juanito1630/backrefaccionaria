const mongoose = require('mongoose');

const schemaProducto = new mongoose.Schema({

  nombre: { type: String, trim: true, required: true, uppercase: true },
  idProducto: { type: String, trim: true },
  descripcion: { type: String, trim: true, uppercase: true },
  familia: { type: String, trim: true, required: true, uppercase: true },
  precioCosto: { type: Number, required: true },
  precioMayoreo: { type: Number, required: true },
  precioVenta: { type: Number, required: true },
  cantidadMinima: { type: Number, required: true },
  cantidadActual: { type: Number, required: true },
  seVendePor: { type: String, trim: true, required: true },
  fechaRegistro: { type: Date, default: new Date()},
  status: { type: String, trim: true, uppercase: true, default: 'ACTIVO' },
}, { timestamps: true });

module.exports = mongoose.model('productos', schemaProducto);
