const mongoose = require('mongoose');

const schemaUser = new mongoose.Schema({
  name: { type: String, trim: true, required: true },
  password: { type: String, trim: true, required: true },
  role: { type: String, trim: true, required: true },

  date: { type: Date, default: Date.now() },
  phone: { type: String, trim: true, required: true},
  email: { type: String, trim: true, required: true, unique: true },
  sexo: { type: String, trim: true, required: true }
});

module.exports = mongoose.model('users', schemaUser);
