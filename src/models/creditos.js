const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SchemaCreditos = new Schema({
  total: { type: Number },
  productos: { type: Array },
  anticipo: { type: Number },
  vendedor: { type: String },
  fechaDeRegistro: { type: Date, default: Date.now() },
  pagos: { type: Array },
  cliente: { type: Schema.Types.ObjectId, ref: 'clientes' },
  status: { type: String , default: 'Abierto' },
  restante: { type: Number, default: 0 }
});

module.exports = mongoose.model('creditos', SchemaCreditos);
