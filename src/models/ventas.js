const mongoose = require('mongoose');

const schemaVentas = new mongoose.Schema({
  vendedor: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
  productos: { type: Array },
  total: { type: Number, required: true },
  metodoPago: { type: String, required: true, trim: true },
  cliente: { type: String, required: true, trim: true },
  monto: { type: Number },
  referenciaTarjeta: { type: String, trim: true },
  montoTarjeta: { type: Number },
});

module.exports = mongoose.model('ventas', schemaVentas);
