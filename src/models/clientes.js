const mongoose = require('mongoose');

const schemaCliente = mongoose.Schema({

  idCliente: { type: String, trim: true },
  nombre: { type: String, trim: true, required: true, uppercase: true },
  telefono: { type: Number, trim: true, required: true },
  correo: { type: String, trim: true, required: true },
  calle: { type: String, trim: true, required: true },
  codigoPostal: { type: Number },
  // colonia: { type: String, trim: true, required: true },
  // municipio: { type: String, trim: true, required: true },
  notas: { type: String, trim: true },
  limiteCredito: { type: Number, required: true },
  // CREDITO
  folio: { type: Number, trim: true, required: true },
  // DATOS DE LA FACTURACION
  RFC: { type: String, trim: true, uppercase: true },
  // razonSocialFacturacion: { type: String, trim: true },
  // correoFacturacion: { type: String, trim: true },
  // calleFacturacion: { type: String, trim: true },
  // coloniaFacturacion: { type: String, trim: true },
  // municipioFacturacion: { type: String, trim: true },
  // codigoPostalFacturacion: { type: Number, trim: true },
  // notasFacturacion: { type: String, trim: true },

}, { timestamps: true });

module.exports = mongoose.model('clientes', schemaCliente);
