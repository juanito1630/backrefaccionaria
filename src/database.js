const mongoose = require('mongoose');
const { URL } = require('./config/index');

const connectDB = async () => {
  console.log(URL);

  
  try {
  
    await mongoose.connect(URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
  
    console.log('DB online');
    return true;
  } catch (error) {
  
    console.log(error);
    
    return false;
    process.exit(1);
  }
};

module.exports = {
  connectDB,
};
