const { Router } = require('express');

const router = Router();

const data = require('../controllers/data');
const home = require('../controllers/index');

router.get('/', home.index);

// RUTAS DE USUARIOS 
router.post('/nuevo/usuario', data.nuevoUsuario);
router.post('/autenticar/usuario', data.autenticarUsuario);
router.get('/obtener/usuarios', data.obtenerUsuarios);
router.delete('/eliminar/usuario/:id', data.eliminarUsuario);
router.get('/ver/usuario/:id', data.buscarUsuarioPorId);
router.put('/actualizar/usuario/:id', data.actualizarUsuaurio);

// RUTAS DE PRODUCTOS
router.get('/obtener/productos', data.obtenerProductos);
router.post('/obtener/producto', data.obtenerProductoPorNombre);
router.post('/obtener/producto/por/id', data.obtenerProductoPorId);
router.post('/registo/producto', data.nuevoProducto);
router.delete('/eliminar/producto/:id', data.eliminarProductoPorId);
router.put('/editar/producto/:id', data.actulizarProducto);


// familias 
router.post('/registro/familia', data.nuevaFamilia);
router.post('/eliminar/familia', data.eliminarFamilia);
router.get('/obtener/familias', data.obtenerFamilias);

// ventas
router.post('/registro/venta', data.nuevaVenta);

router.post('/registro/cliente', data.nuevoCliente);
router.get('/ver/clientes', data.obtenerClientes);
router.get('/cliente/:id', data.cliente);
router.post('/actualizar/cliente', data.actualizarCliente);
router.delete('/eliminar/cliente/:id', data.eliminarCliente);

router.post('/agregar/credito', data.nuevoCredito);
router.get('/ver/creditos', data.verTodosLosCreditos);
router.post('/agregar/abono/:id', data.agregarAbono);
router.get('/ver/abonos/:id', data.buscarAbonoPorId);

module.exports = router;
